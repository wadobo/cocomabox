from django import forms
from client.models import ProductBasket


class AddProductBasketForm(forms.ModelForm):
    class Meta:
        model = ProductBasket
        fields = ['quantity']
