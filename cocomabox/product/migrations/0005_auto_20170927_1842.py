# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-09-27 16:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_auto_20170927_1836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(default='static/images/test/caja.png', upload_to='', verbose_name='Imagen del producto'),
        ),
    ]
