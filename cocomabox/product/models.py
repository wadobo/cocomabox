from django.db import models
import datetime
from provider.models import Provider
from django.core.urlresolvers import reverse
from ckeditor.fields import RichTextField
# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    description = models.TextField(blank=True, null=True,
                                   verbose_name='Descripcion')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]

    def get_absolute_url(self):
        view_name = 'products_by_category'
        return reverse(view_name, kwargs={"pk": self.pk})


class Product(models.Model):

    UNITS = (('kg', 'kilogramo'),
             ('g', 'gramo'),
             ('l', 'litro'),
             ('ml', 'mililitro'),
             ('ud', 'unidad'), )

    name = models.CharField(max_length=100, verbose_name='Nombre')
    provider = models.ForeignKey(Provider, related_name='product',
                                 default=None,
                                 verbose_name='Proveedor')
    category = models.ForeignKey(Category, related_name='product',
                                 default=None,
                                 verbose_name='Categoria')
    quantity = models.FloatField(default=1.00, verbose_name='Cantidad')
    minimum_quantity = models.FloatField(default=0.00, verbose_name='Cantidad mínima')
    price = models.FloatField(default=0.00, verbose_name='Precio')
    description = RichTextField(default=None, blank=True)
    image = models.ImageField(upload_to='',
                              default='caja.png',
                              verbose_name='Imagen del producto')
    available = models.BooleanField(default=True, verbose_name='Disponible')
    bulk = models.BooleanField(default=True, verbose_name='Granel')
    unit = models.CharField(max_length=15, choices=UNITS, default=UNITS[0],
                            verbose_name='Unidad')
    example = models.CharField(blank=True, default=None, null=True,
                               max_length=50,
                               verbose_name='Ejemplo')

    def __str__(self):
        return "%s (%s €)" % (self.name, self.price)

    def get_absolute_url(self):
        view_name = 'product'
        return reverse(view_name, kwargs={"pk": self.pk})

    class Meta:
        ordering = ["name"]


class ProductDiscount(models.Model):
    code = models.CharField(max_length=50, blank=False, unique=True, null=False, verbose_name='Código')
    start_date = models.DateTimeField(auto_now=False,
                                      verbose_name='Inicio del descuento',
                                      default=(datetime.datetime.today()))
    end_date = models.DateTimeField(auto_now=False,
                                    verbose_name='Fin del descuento')
    products = models.ManyToManyField(Product, verbose_name='Productos', blank=True)
    categories = models.ManyToManyField(Category, verbose_name='Categorias', blank=True)
    quantity = models.FloatField(default=0.00, verbose_name='Cantidad de descuento')
    percent = models.BooleanField(default=True, verbose_name='Habilitar porcentaje')
    available = models.BooleanField(default=True, verbose_name='Descuento habilitado')
    one_by_client = models.BooleanField(default=True, verbose_name='Un uso por cliente')

    def __str__(self):
        return '%s' % (self.code)
