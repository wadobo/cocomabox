from django.contrib import admin
from product.models import Product, Category, ProductDiscount

# Register your models here.


def products_available(modeladmin, request, queryset):
    queryset.update(available=True)


products_available.short_description = "Mark products as available"


def products_not_available(modeladmin, request, queryset):
    queryset.update(available=False)


products_not_available.short_description = "Mark products as NOT available"


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'provider', 'quantity', 'price',
                    'unit', 'bulk', 'available', ]
    list_filter = ['provider', 'category']
    search_fields = ['name']
    list_editable = ['quantity', 'price']
    ordering = ['category']
    actions = [products_available, products_not_available]


admin.site.register(Category)

@admin.register(ProductDiscount)
class ProductDiscountAdmin(admin.ModelAdmin):
    list_display = ['code', 'start_date', 'end_date', 'quantity',
                    'percent', 'available', ]
    search_fields = ['code']
    list_filter = ['available']
    list_editable = ['available']
    ordering = ['start_date', 'end_date']




#admin.site.register(ProductDiscount)
