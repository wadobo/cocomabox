from django.shortcuts import redirect, render
from cocomabox.views import get_client_login, IsCycleOpen, msgClientProfile
from product.models import Category, Product
from client.models import ProductBasket
from django.views.generic import TemplateView, ListView
from product.forms import AddProductBasketForm
from product.models import Product
from django.utils.decorators import method_decorator
# Create your views here.


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class Products(ListView):
    queryset = Product.objects.filter(available=True)
    template_name = 'products.html'
    paginate_by = 9

    def get_context_data(self, *args, **kwargs):
        ctx = super(Products, self).get_context_data(*args, **kwargs)
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class ProductView(TemplateView):
    template_name = 'product.html'
    
    error = ''
    
    def post(self, *args, **kwargs):
        ctx = self.get_context_data()
        product = ctx['product']
        quantity = float(self.request.POST['quantity'])
        client = ctx['client']
        if quantity >= product.minimum_quantity:
            ProductBasket.add_product(client, product, quantity)
            return redirect('added_product')
        else:
            self.error = 'Cantidad mínima: %s %s ' % (product.minimum_quantity, product.unit)
            return render(self.request,
                          self.template_name,
                          self.get_context_data())

    def get_context_data(self, *args, **kwargs):
        ctx = super(ProductView, self).get_context_data(*args, **kwargs)
        ctx['error'] = self.error
        self.error = ''
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        ctx['form'] = AddProductBasketForm()
        ctx['product'] = Product.objects.get(pk=self.kwargs['pk'])
        return ctx


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class ProductsByCategory(ListView):
    queryset = Product.objects.filter(available=True)
    template_name = 'products.html'
    paginate_by = 9

    def get_queryset(self):
        c = Category.objects.get(pk=self.kwargs['pk'])
        return Product.objects.filter(available=True)\
                              .filter(category=c)

    def get_context_data(self, *args, **kwargs):
        ctx = super(ProductsByCategory, self).get_context_data(*args, **kwargs)
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx
