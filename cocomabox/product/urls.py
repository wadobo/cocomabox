from django.conf.urls import url
from product.views import ProductsByCategory, ProductView


urlpatterns = [
    url(r'^categoria/(?P<pk>\d+)$', ProductsByCategory.as_view(),
        name='products_by_category'),
    url(r'^producto/(?P<pk>\d+)', ProductView.as_view(),
        name='product'),
]
