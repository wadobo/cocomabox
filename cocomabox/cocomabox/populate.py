from django.contrib.auth.models import User
from client.models import Client
from provider.models import Provider
from product.models import Category
from product.models import Product


clients = {'cliente1': None, 'cliente2': None, 'cliente3': None,
           'cliente4': None, 'cliente5': None, 'cliente6': None, }

for c in clients:
    try:
        u = User.objects.create_user(username=c,
                                     email='',
                                     password='qweqweqwe')
        Client.objects.create(user=u,
                              first_name=c,
                              last_name=c + ' ' + c,
                              phone='000',
                              email=c + '@cliente.org')
    except:
        print('El ' + c + ' no pudo ser creado.')


providers = {'Proveedor1': ['La huerta', 'Manuel', 'González Jiménez',
                            'C. Constitución n3', '654654654', '12452145D',
                            'contacto@lahuerta.org', 'www.lahuerta.org'],
             'Proveedor2': ['El valle', 'Pepe', 'Arellano Santana',
                            'Av. La republica', '741741741', '74589612Q',
                            'contacto@elvalle.org', 'www.elvalle.org'],
             'Proveedor3': ['La encina', 'Pedro', 'Paulino Cáceres',
                            'C. Manuel Hernández n9', '698745123', '12546389G',
                            'contacto@laencina.org', 'www.laencina.org'],
             }

for p in providers:
    try:
        u = User.objects.create_user(username=p,
                                     email='',
                                     password='qweqweqwe')
        pro = Provider.objects.create(user=u,
                                      company=providers[p][0],
                                      first_name=providers[p][1],
                                      last_name=providers[p][2],
                                      address=providers[p][3],
                                      phone=providers[p][4],
                                      cif=providers[p][5],
                                      email=providers[p][6],
                                      web=providers[p][7])
        pro.save()
    except:
        print('El ' + p + ' no pudo ser creado.')


categories = ['Hortalizas', 'Legumbres', 'Lácteos',
              'Fruta', 'verduras', 'origen animal']
for c in categories:
    if not Category.objects.all().filter(name=c):
        try:
            Category.objects.create(name=c)
        except:
            print('La categoria ' + c + ' no pudo ser creada.')


products = {'Patatas': ['Patatas',
                        Category.objects.get(pk=1),
                        Provider.objects.get(pk=1)],
            'Manzanas': ['Manzanas',
                         Category.objects.get(pk=4),
                         Provider.objects.get(pk=2)]

            }

for p in products:
    if not Product.objects.all().filter(name=p):
        try:
            Product.objects.create(name=products[p][0],
                                   category=products[p][1],
                                   provider=products[p][2])
        except:
            print('El producto ' + p + ' no pudo ser creado.')

    # name =
    # provider =
    # category =
