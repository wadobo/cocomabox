"""cocomabox URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from product.views import Products
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', Products.as_view(), name='products'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^utils/', include('utils.urls')),
    url(r'^cliente/', include('client.urls')),
    url(r'^producto/', include('product.urls')),
    url(r'^pedidos/', include('order.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_title = u'Cocoma Box'
admin.site.site_header = u'Cocoma Box'
admin.site.index_title = u'Panel de control de Cocoma Box'
