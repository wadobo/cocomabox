from django import forms
from client.models import ProductBasket


class ContactForm(forms.Form):
    topic = forms.CharField(required=True,
                            initial="Escríbenos un título para tu opinión",
                            max_length=100)
    message = forms.CharField(required=True, widget=forms.Textarea(),
                              initial="Escríbenos tu opinión")
    sender = forms.EmailField(required=False)
