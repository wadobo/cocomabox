from django.shortcuts import render, redirect
from django.db.models import Q
from django.http import HttpResponseRedirect
from product.models import Product, Category
from utils.models import Cycle
from client.models import ProductBasket, Client
from django.contrib.auth.models import User
from cocomabox.forms import ContactForm
from django.core.mail import send_mail
from django.conf import settings
from django.views.generic import TemplateView, ListView
from django.utils.decorators import method_decorator
from django.contrib.auth import logout as auth_logout
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.contrib import messages

# from order.models import Order

# messages.info(self.request, 'Info CABESA')
# messages.error(self.request, 'Error CABESA')


class msgClientProfile(object):
    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        c = get_client_login(args[0])
        if c:
            if (not c.first_name or not c.last_name or not c.phone or
                    not c.email):
                messages.error(args[0], 'Debe completar su <a href=' +
                               c.get_absolute_url() +
                               '>perfil</a> de usuario.')

        return self.function(*args, **kwargs)


class IsCycleOpen(object):

    def __init__(self, function):
        self.function = function

    def __call__(self, *args, **kwargs):
        if Cycle.get_cycle():
            return self.function(*args, **kwargs)

        return redirect('closed_catalogue')


def get_client_login(request):
    if request.user.is_authenticated:
        return Client.objects.filter(
                user=User.objects.get(pk=request.user.pk)).first()


@method_decorator(msgClientProfile, name='get')
class ClosedCatalogue(TemplateView):
    template_name = 'closed_catalogue.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(ClosedCatalogue, self).get_context_data(*args, **kwargs)
        ctx['cycle'] = Cycle.get_next_cycle()
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx


@method_decorator(msgClientProfile, name='get')
class Thanks(TemplateView):
    template_name = 'thanks.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(Thanks, self).get_context_data(*args, **kwargs)
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class Search(ListView):
    template_name = 'products.html'
    paginate_by = 9

    def get_queryset(self):
        query = self.request.GET.get('q', '')
        if query:
            qset = (Q(name__icontains=query) | Q(description__icontains=query))
            results = Product.objects.filter(qset).filter(
                        available=True).distinct()
        else:
            results = []
        return results

    def get_context_data(self, *args, **kwargs):
        ctx = super(Search, self).get_context_data(*args, **kwargs)
        query = self.request.GET.get('q', '')
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        ctx['query'] = query
        return ctx


def send_mail_order(client, order):
    topic = 'Cocomabox Nº Pedido ' + str(order.pk) + ' ' + str(order.date)
    message = 'Cliente: ' + order.client.first_name + ' ' +\
        order.client.last_name + '\n\n'

    message += 'Producto\t\t Cantidad\t\t Precio\n'
    message += '------------------------------------------------------\n'
    for p in ProductBasket.by_client(order.client):
        message += str(p.product.name) + '\t\t\t' +\
                   str(p.quantity) + '\t\t\t' +\
                   str(p.product.price) + '\n'

    message += 'Total:   ' + str(ProductBasket.get_total(order.client)) + '\n'
    sender = str(order.client.email)
    send_mail(topic, message, settings.DEFAULT_FROM_EMAIL, [sender])


def send_mail_order(client, order):
    from order.views import pdf_client_order
    from order.models import Order
    topic = 'Cocomabox Nº Pedido ' + str(order.pk) + ' ' + str(order.date)
    message = 'Cliente: ' + order.client.first_name + ' ' +\
        order.client.last_name + '\n\n'

    sender = str(order.client.email)
    msg = EmailMessage(topic, message, to=[sender])
    pdf = pdf_client_order(Order.objects.filter(pk=order.pk))
    msg.attach('pedido.pdf', pdf, 'application/pdf')
    msg.content_subtype = "html"
    msg.send()


def send_new_catalog(client, request):
    cycle = Cycle.get_cycle()
    site = Site.objects.all().first()
    topic = 'Nuevo catálogo Cocomá Box ' +\
            cycle.open_cycle.strftime("%d/%m/%y") + ' ' +\
            cycle.close_cycle.strftime("%d/%m/%y")
    message = 'Estimado/a ' + client.first_name + ',\n\n'
    message += 'Los agricultores acaban de registrar todos los productos disponibles para la semana que viene, ya puede empezara hacer su pedido.'
    message += '\n\n'
    message += 'Gracias por su confianza, \n\n'
    message += 'Cocomá.'
    sender = str(client.email)
    send_mail(topic, message, settings.DEFAULT_FROM_EMAIL, [sender])


def contact(request):
    form = ContactForm(request.POST)
    client = get_client_login(request)
    context = {'category': Category.objects.all(),
               'client': client,
               'form': '',
               'basket_quantity': ProductBasket.by_client(client).count(), }
    if request.method == 'POST' and form.is_valid():
            topic = form.cleaned_data['topic']
            message = form.cleaned_data['message']
            sender = form.cleaned_data.get('sender', 'noreply@example.com')
            send_mail(topic, message, sender, [settings.DEFAULT_FROM_EMAIL])
            return redirect('thanks')
    else:
        context['form'] = ContactForm()
    return render(request, 'contact.html', context)
