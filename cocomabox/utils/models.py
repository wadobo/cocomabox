from django.db import models
import datetime
from product.models import Product
from provider.models import Provider
from django.utils import timezone


# Create your models here.

DAYS = (('Monday', 'Monday'),
        ('Tuesday', 'Tuesday'),
        ('Wednesday', 'Wednesday'),
        ('Thurday', 'Thurday'),
        ('Friday', 'Friday'),
        ('Saturday', 'Saturday'),
        ('Sunday', 'Sunday'))


class Cycle(models.Model):

    open_cycle = models.DateTimeField(auto_now=False,
                                      verbose_name='Apertura de catálogo',
                                      default=(datetime.datetime.today()))
    close_cycle = models.DateTimeField(auto_now=False,
                                       verbose_name='Cierre de catálogo',
                                       default=(datetime.datetime.today() +
                                                datetime.timedelta(days=5)))
    pickup_day_a = models.DateTimeField(auto_now=False,
                                        verbose_name='Día de recogida A',
                                        default=(datetime.datetime.today() +
                                                 datetime.timedelta(days=9)))
    pickup_day_b = models.DateTimeField(auto_now=False,
                                        verbose_name='Día de recogida B',
                                        default=None,
                                        null=True,
                                        blank=True)

    class Meta:
        ordering = ['-close_cycle']
        verbose_name_plural = 'Ciclos'

    def save(self, *args, **kwargs):
        super(Cycle, self).save(*args, **kwargs)
        for prov in Provider.objects.all():
            if not ProviderOrder.objects.filter(cycle=self, provider=prov):
                ProviderOrder.objects.create(cycle=self, provider=prov,
                                             total=0.0)

    def __str__(self):
        return '%s' % \
            (self.open_cycle.strftime("%d/%m/%y"))

    def get_date(self):
        return self.close_cycle.strftime("%d/%m/%y")

    def get_cycle():
        today = timezone.now()
        for c in Cycle.objects.all():
            if today > c.open_cycle and today < c.close_cycle:
                return c
        return None

    def get_next_cycle():
        today = timezone.now()
        for c in Cycle.objects.all():
            if today < c.open_cycle:
                return c

        return None


class ProviderOrder(models.Model):
    cycle = models.ForeignKey(Cycle, blank=True, default=None)
    provider = models.ForeignKey(Provider, blank=True)
    total = models.FloatField(default=0.0)

    def set_total(self):
        l_prod = ProductProviderOrder.objects.all().filter(providerOrder=self)
        total = 0.0
        for p in l_prod:
            total += p.get_total()

        self.total = total
        self.save()

    def __str__(self):
        return "%s" % (self.provider)

    def by_order(self):
        return ProductProviderOrder.objects.all().filter(providerOrder=self)


class ProductProviderOrder(models.Model):
    providerOrder = models.ForeignKey(ProviderOrder, blank=True)
    product = models.ForeignKey(Product, blank=True)
    quantity = models.FloatField()

    def get_total(self):
            total = self.product.price * self.quantity
            return round(total, 2)

    def __str__(self):
        return "Unidad: %s" % (self.product.unit)
