# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-08-24 17:35
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0007_auto_20170716_2254'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='close_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 8, 29, 19, 35, 40, 677660), verbose_name='Cierre de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='open_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 8, 24, 19, 35, 40, 677621), verbose_name='Apertura de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='pickup_day_a',
            field=models.DateTimeField(default=datetime.datetime(2017, 9, 2, 19, 35, 40, 677690), verbose_name='Día de recogida A'),
        ),
    ]
