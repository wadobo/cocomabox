# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-03 09:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0012_auto_20171003_0953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='close_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 8, 11, 26, 48, 11274), verbose_name='Cierre de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='open_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 3, 11, 26, 48, 11226), verbose_name='Apertura de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='pickup_day_a',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 12, 11, 26, 48, 11304), verbose_name='Día de recogida A'),
        ),
    ]
