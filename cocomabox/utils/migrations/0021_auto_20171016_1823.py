# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-16 16:23
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0020_auto_20171016_1716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='close_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 21, 18, 23, 51, 124223), verbose_name='Cierre de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='open_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 16, 18, 23, 51, 124186), verbose_name='Apertura de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='pickup_day_a',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 25, 18, 23, 51, 124256), verbose_name='Día de recogida A'),
        ),
    ]
