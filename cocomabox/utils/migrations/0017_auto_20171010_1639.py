# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-10-10 14:39
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utils', '0016_auto_20171003_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cycle',
            name='close_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 15, 16, 39, 49, 556794), verbose_name='Cierre de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='open_cycle',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 10, 16, 39, 49, 556744), verbose_name='Apertura de catálogo'),
        ),
        migrations.AlterField(
            model_name='cycle',
            name='pickup_day_a',
            field=models.DateTimeField(default=datetime.datetime(2017, 10, 19, 16, 39, 49, 556831), verbose_name='Día de recogida A'),
        ),
    ]
