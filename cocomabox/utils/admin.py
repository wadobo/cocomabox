from django.contrib import admin
from utils.models import Cycle, ProviderOrder, ProductProviderOrder
from django import forms
from order.views import pdf_provider_order_response

# Register your models here


@admin.register(Cycle)
class CycleAdmin(admin.ModelAdmin):
    list_display = ['open_cycle', 'close_cycle', 'pickup_day_a',
                    'pickup_day_b']
    list_filter = ['close_cycle', 'open_cycle']
    search_fields = ['close_cycle']
    ordering = ['-close_cycle']
    save_as = True


class ProductProviderOrderInline(admin.TabularInline):
    model = ProductProviderOrder


def pdf_order_action(modeladmin, request, queryset):
    return pdf_provider_order_response(request, queryset)


pdf_order_action.short_description = "Generate pdf order"


@admin.register(ProviderOrder)
class ProviderOrderAdmin(admin.ModelAdmin):
    list_display = ['cycle', 'provider', 'total']
    list_filter = ['provider', 'cycle']
    ordering = ['cycle', 'provider', 'total']
    search_fields = ['provider__company', 'cycle__close_cycle']
    actions = [pdf_order_action]
    inlines = [ProductProviderOrderInline]
