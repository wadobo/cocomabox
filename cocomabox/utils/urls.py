from django.conf.urls import url
from cocomabox.views import ClosedCatalogue, contact, Thanks, Search

urlpatterns = [
    url(r'^cerrado/$', ClosedCatalogue.as_view(), name='closed_catalogue'),
    url(r'^contacto/$', contact, name='contact'),
    url(r'^contacto/gracias/$', Thanks.as_view(), name='thanks'),
    url(r'^busqueda/$', Search.as_view(), name='search'),
]
