from django.contrib import admin
from provider.models import Provider
# Register your models here.


@admin.register(Provider)
class ProviderAdmin(admin.ModelAdmin):
    list_display = ['company', 'phone', 'email', 'cif']
    search_fields = ['company']
    ordering = ['company']
