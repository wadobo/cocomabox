from django.db import models


# Create your models here.


class Provider(models.Model):
    company = models.CharField(max_length=100, null=True,
                               verbose_name='Nombre de la empresa')
    first_name = models.CharField(max_length=100, verbose_name='Nombre')
    last_name = models.CharField(max_length=100, verbose_name='Apellidos')
    address = models.CharField(max_length=100, verbose_name='Dirección')
    phone = models.CharField(max_length=100, verbose_name='Teléfono')
    cif = models.CharField(max_length=100, verbose_name='CIF')
    email = models.EmailField(verbose_name='Email')
    web = models.URLField(verbose_name='Sitio web')
    observations = models.TextField(blank=True, null=True,
                                    verbose_name='Observaciones')

    def __str__(self):
        return '%s' % (self.company)

    class Meta:
        ordering = ["company"]
