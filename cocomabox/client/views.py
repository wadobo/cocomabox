from django.shortcuts import render, redirect
from client.models import Client, ProductBasket
from product.models import Product, Category, ProductDiscount
from utils.models import Cycle
from order.models import Order
from cocomabox.views import get_client_login, IsCycleOpen, msgClientProfile, send_mail_order
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView, UpdateView
from django.utils.decorators import method_decorator

# Create your views here.


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class ClientBasket(LoginRequiredMixin, TemplateView):
    template_name = 'client_basket.html'
    
    error = {}
    discount_error = False 

    def post(self, *args, **kargs):
        ctx = self.get_context_data()
        client = ctx['client']
        if 'update' in self.request.POST:
            product = Product.objects.get(pk=int(self.request.POST['product']))
            pbasket = ProductBasket.by_client(client).filter(product=product)
            pbasket = pbasket.first()
            quantity = float(self.request.POST['quantity'].replace(',', '.'))
            if quantity >= product.minimum_quantity:
                ProductBasket.set_product_quantity(pbasket, quantity)
            else:
                self.error[product.pk] = 'Cantidad mínima: %s %s ' % (product.minimum_quantity, product.unit)
                ProductBasket.set_product_quantity(pbasket, product.minimum_quantity)

        if 'remove' in self.request.POST:
            remove_product_basket(self.request)

        if 'make-order' in self.request.POST:
            make_order(self.request)
            return redirect('order_requested')
        
        if 'discount' in self.request.POST:
            self.request.session['discount'] = self.request.POST['discount']
            if not check_discount(self.request, self.request.POST['discount']):
                self.discount_error = True

        return render(self.request,
                      self.template_name,
                      self.get_context_data())

    def get(self, *args, **kwargs):
        return render(self.request,
                      self.template_name,
                      self.get_context_data())

    def get_context_data(self, *args, **kwargs):
        ctx = super(ClientBasket, self).get_context_data(*args, **kwargs)
        client = get_client_login(self.request)
        ctx['error'] = self.error
        ctx['discount_error'] = self.discount_error
        self.error = {}
        if 'discount' in self.request.session.keys():
            if ProductDiscount.objects.filter(code=self.request.session['discount']).first():
                discount = ProductDiscount.objects.get(code=self.request.session['discount'])
                ctx['discount'] = discount.code
                ctx['total_discount'] = ProductBasket.get_total_discount(client, discount)
            else:
                ctx['discount'] = self.request.session['discount']
        ctx['category'] = Category.objects.all()
        ctx['client'] = client
        ctx['products'] = ProductBasket.by_client(client)
        ctx['basket_quantity'] = ProductBasket.by_client(client).count()
        ctx['total'] = ProductBasket.get_total(client)
        ctx['cycle'] = Cycle.get_cycle()
        return ctx


@method_decorator(msgClientProfile, name='get')
class ClientProfile(LoginRequiredMixin, UpdateView):
    model = Client
    fields = ['first_name', 'last_name', 'phone', 'email']
    template_name = 'client_profile.html'
    login_url = '/accounts/login/'

    def get_context_data(self, *args, **kwargs):
        ctx = super(ClientProfile, self).get_context_data(*args, **kwargs)
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx


def export_email_client(request, queryset):
    ctx = {'clients': queryset}
    return render(request, 'email_client_list.html', ctx) 

@method_decorator(IsCycleOpen, name='dispatch')
class AddedProduct(TemplateView):

    def get(self, *args, **kwargs):
        return redirect('products')

    def get_context_data(self, *args, **kwargs):
        ctx = super(AddedProduct, self).get_context_data(*args, **kwargs)
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx


def check_discount(request, discount):
    client = get_client_login(request) 
    try:
        pd = ProductDiscount.objects.filter(code=discount).first()
        if not Order.objects.filter(client=client).filter(discount=pd):
            return True
        else:
            return False
    except:
        return False


def remove_product_basket(request):
    client = get_client_login(request)
    ProductBasket.remove_product(client,
                                 Product.objects.get(
                                    pk=request.POST['product']))
    return redirect('client_basket')


def make_order(request):
    client = get_client_login(request)
    try:
        if request.POST['pickup_day'] == 'pickup_day_a':
            pickup_day = Cycle.get_cycle().pickup_day_a
        elif request.POST['pickup_day'] == 'pickup_day_b':
            pickup_day = Cycle.get_cycle().pickup_day_b

        if request.POST['discount'] != '' and check_discount(request, request.POST['discount']):
            discount = ProductDiscount.objects.get(code=request.POST['discount'])
        else:
            discount = None

        if discount:
            total_discount = ProductBasket.get_total_discount(client, discount)
        else:
            total_discount = 0.00
        if request.POST['comment']:
            observations = request.POST['comment']
        else:
            observations = ''
            
        order = Order.objects.create(client=client,
                                     total=ProductBasket.get_total(client),
                                     total_discount=total_discount,
                                     pickup_day=pickup_day,
                                     observations=observations,
                                     discount=discount)
        order.add_products()
        send_mail_order(client, order)
        del(request.session['discount'])
        
    except:
        pass
    return redirect('client_basket')
