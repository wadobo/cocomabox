from django.conf.urls import url
from client.views import ClientProfile, ClientBasket, AddedProduct

urlpatterns = [
    url(r'^perfil/(?P<pk>\d+)', ClientProfile.as_view(),
        name='client_profile'),
    url(r'^cesta/$', ClientBasket.as_view(), name='client_basket'),
    url(r'^producto-agregado/$', AddedProduct.as_view(),
        name='added_product'),
]
