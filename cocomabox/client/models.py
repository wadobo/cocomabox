from django.db import models
from django.contrib.auth.models import User
from product.models import Product
from django.core.urlresolvers import reverse
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
# Create your models here.


class Client(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE,
                                related_name="client", verbose_name='Usuario')
    first_name = models.CharField(max_length=100, verbose_name='Nombre')
    last_name = models.CharField(max_length=100, verbose_name='Apellidos')
    phone = models.CharField(max_length=100, verbose_name='Teléfono')
    email = models.EmailField()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)

    class Meta:
        ordering = ["first_name"]

    def get_absolute_url(self):
        view_name = 'client_profile'
        return reverse(view_name, kwargs={"pk": self.pk})

    @property
    def admin_name(self):
        return '%s, %s' % (self.last_name, self.first_name)


@receiver(post_save, sender=User, dispatch_uid="user_create_client")
def user_create_client(sender, instance, **kwargs):
    c = Client.objects.filter(user=instance).first()
    if not c:
        c = Client.objects.create(user=instance)
        c.save()

        c.first_name = instance.first_name
        c.last_name = instance.last_name
        c.email = instance.email
        c.save()


def quantity_minimum(value):
    if value % 2 != 0:
        raise ValidationError(
            _('%(value)s is not an even number'),
            params={'value': value},
        )

class ProductBasket(models.Model):
    client = models.ForeignKey(Client, blank=True)
    product = models.ForeignKey(Product, blank=True, verbose_name='Producto')
    quantity = models.FloatField(verbose_name='Cantidad')

    def by_client(c):
        return ProductBasket.objects.all().filter(client=c)

    def add_product(c, p, q):
        if q > 0:
            product_list = ProductBasket.by_client(c)
            product_exist = product_list.filter(product=p)
            if product_exist:
                pr1 = product_exist.first()
                pr1.quantity += float(q)
                pr1.save()
            else:
                ProductBasket.objects.create(client=c, product=p, quantity=q)

    def product_quantity(c, p):
        pd = ProductBasket.by_client(c)
        product = pd.filter(product=p)
        return product.quantity

    def set_product_quantity(self, q):
        self.quantity = q
        self.save()

    def remove_product(c, p):
        ProductBasket.by_client(c).filter(product=p).delete()

    def clean_basket(c):
        ProductBasket.by_client(c).delete()

    def get_total(c):
        total = 0
        for p in ProductBasket.by_client(c):
            total += p.product.price * p.quantity
        return round(total, 2)

    def get_total_discount(client, discount):
        total = 0
        if not discount.categories.all() and not discount.products.all():
            for p in ProductBasket.by_client(client):
                total += p.product.price * p.quantity
            if discount.percent:
                total = total - (total * discount.quantity)/100
            else:
                total = total - discount.quantity
        else:
            for p in ProductBasket.by_client(client):
                p_total = 0
                if p.product.category in discount.categories.all() or p.product in discount.products.all():
                    p_price = p.product.price * p.quantity
                    if discount.percent:
                        total += p_price - (p_price * discount.quantity)/100 
                    else:
                        total += p_price - discount.quantity
                else:
                    total += p.product.price * p.quantity
                
        return round(total, 2)

    
    def __str__(self):
        return '%s %s' % (self.product, self.quantity)
