from django.contrib import admin
from client.models import Client
from client.models import ProductBasket
from cocomabox.views import send_new_catalog
from client.views import export_email_client
# Register your models here.


class ProductBasketInline(admin.TabularInline):
    model = ProductBasket


def new_catalog(modeladmin, request, queryset):
    for client in queryset:
        send_new_catalog(client, request)

def export_email_client_action(modeladmin, request, queryset):
    return export_email_client(request, queryset)


new_catalog.short_description = "New catalog"


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['admin_name', 'phone', 'email']
    inlines = [ProductBasketInline, ]
    actions = [new_catalog, export_email_client_action]
    ordering = ['last_name']
    save_as = True
    # save_on_top = True

    class Meta:
        model = Client
