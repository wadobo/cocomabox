from django import forms

from registration.forms import RegistrationForm


class CustomRegForm(RegistrationForm):
    first_name = forms.CharField(label=u'Nombre', max_length=30)
    last_name = forms.CharField(label=u'Apellidos', max_length=50)
    phone = forms.CharField(label=u'Teléfono', max_length=20)

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        obj.client.first_name = self.cleaned_data['first_name']
        obj.client.last_name = self.cleaned_data['last_name']
        obj.client.phone = self.cleaned_data['phone']
        obj.client.save()

        return obj
