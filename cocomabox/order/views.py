from django.shortcuts import redirect, render
from product.models import Category
from order.models import Order, ProductOrder
from client.models import ProductBasket
from cocomabox.views import get_client_login, IsCycleOpen, msgClientProfile, send_mail_order
from utils.models import Cycle
from django.views.generic import TemplateView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.decorators import method_decorator
from reportlab.pdfgen import canvas
from django.http import HttpResponse
from io import BytesIO
from reportlab.platypus import Table
from reportlab.platypus import SimpleDocTemplate, Paragraph, TableStyle, Image
from reportlab.lib import colors

from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

from reportlab.platypus import SimpleDocTemplate, Paragraph, PageBreak
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER
from django.contrib.auth.models import User
from reportlab.lib import utils
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
# Create your views here.


def get_image(path, width=3*cm):
    path = path.encode('utf8')
    img = utils.ImageReader(path)
    iw, ih = img.getSize()
    aspect = ih / float(iw)
    return Image(path, width=width, height=(width * aspect))


def pdf_client_order(queryset):
    buffer = BytesIO()
    doc = SimpleDocTemplate(buffer,
                            rightMargin=72,
                            leftMargin=72,
                            topMargin=72,
                            bottomMargin=72,
                            pagesize=A4)
    elements = []
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
    for order in queryset:

        table_client = []
        logo = get_image("static/images/cocoma_logo_menu.png", 100)
        datos_empresa = []
        datos_empresa.append(['Cocoma S.C.A.'])
        datos_empresa.append(['CIF: F90292491'])
        datos_empresa.append(['Calle Pablo Coso Calero'])
        datos_empresa.append(['Edificio Geseduma 1, Local 5'])
        datos_empresa.append(['41930, Bormujos, Sevilla, España'])
        table_client.append([logo, '', Table(datos_empresa, rowHeights=13)])
        table_client.append(['Numero de pedido: ' + str(order.pk),
                             'Pedido:' + order.date.strftime("%d/%m/%y %H:%M"),
                             'Recogida: ' +
                             order.pickup_day.strftime("%d/%m/%y")
                             ])
        table_client.append([str(order.client.pk) + ' ' +
                             order.client.first_name + ' ' +
                             order.client.last_name,
                             'Teléfono: ' + order.client.phone,
                             'Email: ' + order.client.email])
        elements.append(Table(table_client, colWidths=[doc.width/3.0]*3))
        table_data = []
        table_data.append(['ID Producto', 'Producto', 'Precio', 'Cantidad',
                           'Total'])
        for p in order.get_products():
            table_data.append([p.product.pk, p.product.name,
                               str(p.product.price) + '€/' + p.product.unit,
                               str(p.quantity) + ' ' + p.product.unit,
                               str(round(p.product.price * p.quantity, 2)) +
                               '€'])

        product_table = Table(table_data, colWidths=[70, 180, 70, 70, 60])
        product_table.setStyle(TableStyle([('ALIGN', (0, 0), (3, 0), 'CENTER'),
                                           ('GRID', (0, 0), (-1, -1), 1,
                                           colors.black),
                                           ('FONTSIZE', (0, 0), (-1, -1), 10),
                                           ]))
        linebreak = Table(['\n'], rowHeights=13)
        table_total = []
        table_total.append(['', 'Total:', str(order.total)])
        if order.discount:
            table_total.append(['Código descuento: {}'.format(order.discount), 'Total:', str(order.total_discount)])
        elements.append(linebreak)
        elements.append(product_table)
        elements.append(linebreak)
        elements.append(Table(table_total, colWidths=[320, 70, 60]))
        styleAviso = ParagraphStyle(name="centerdStyle",
                                fontSize=10,
                                alignment=TA_CENTER)
        elements.append(Paragraph('<b>** El precio indicado se trata de una estimación **</b>', styleAviso))
        elements.append(linebreak)
        if order.observations:
            styleC = ParagraphStyle(name="centerdStyle",
                                    fontSize=10,
                                    alignment=TA_LEFT)
            elements.append(Paragraph('<b>Observaciones:</b>', styleC))
            elements.append(Paragraph(order.observations, styleC))

        elements.append(PageBreak())
    doc.build(elements)
    pdf = buffer.getvalue()
    buffer.close()
    return pdf


def pdf_provider_order(queryset):
    buffer = BytesIO()
    doc = SimpleDocTemplate(buffer,
                            rightMargin=72,
                            leftMargin=72,
                            topMargin=72,
                            bottomMargin=72,
                            pagesize=A4)
    elements = []
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='centered', alignment=TA_CENTER))
    for prov_order in queryset:

        table_client = []
        logo = get_image("static/images/cocoma_logo_menu.png", 75)
        datos_empresa = []
        datos_empresa.append(['Cocoma S.C.A.'])
        datos_empresa.append(['CIF: F90292491'])
        datos_empresa.append(['Calle Pablo Coso Calero'])
        datos_empresa.append(['Edificio Geseduma 1, Local 5'])
        datos_empresa.append(['41930, Bormujos, Sevilla, España'])
        table_client.append([logo, '', Table(datos_empresa, rowHeights=13)])
        table_client.append(["Número de pedido: " + str(prov_order.pk),
                             "Apertura: " +
                             prov_order.cycle.open_cycle.strftime("%d/%m/%y"),
                             "Cierre: " +
                             prov_order.cycle.close_cycle.strftime("%d/%m/%y")
                             ])
        table_client.append(["Proveedor: " + str(prov_order.provider.company),
                             "Teléfono: " + prov_order.provider.phone,
                             "Email: " + prov_order.provider.email
                             ])

        elements.append(Table(table_client, colWidths=[doc.width/3.0]*3))
        table_data = []
        table_data.append(['ID Producto', 'Producto', 'Precio', 'Cantidad',
                           'Total'])
        for p in prov_order.by_order():
            table_data.append([p.product.pk, p.product.name,
                               str(p.product.price) + '€/' + p.product.unit,
                               str(p.quantity) + ' ' + p.product.unit,
                               str(round(p.product.price * p.quantity, 2)) +
                               '€'])

        product_table = Table(table_data, colWidths=[70, 180, 70, 70, 60])
        product_table.setStyle(TableStyle([('ALIGN', (0, 0), (3, 0), 'CENTER'),
                                           ('GRID', (0, 0), (-1, -1), 1,
                                           colors.black),
                                           ('FONTSIZE', (0, 0), (-1, -1), 10),
                                           ]))
        linebreak = Table(['\n'], rowHeights=13)
        table_total = []
        table_total.append(['', 'Total:', str(prov_order.total)])
        elements.append(linebreak)
        elements.append(product_table)
        elements.append(linebreak)
        elements.append(Table(table_total, colWidths=[320, 70, 60]))
        elements.append(PageBreak())
    doc.build(elements)
    pdf = buffer.getvalue()
    buffer.close()
    return pdf


def pdf_client_order_response(request, queryset):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="pedidos_clientes".pdf"'
    response.write(pdf_client_order(queryset))
    return response


def pdf_provider_order_response(request, queryset):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="pedidos_proveedores".pdf"'
    response.write(pdf_provider_order(queryset))
    return response


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class OrderRequested(TemplateView):
    template_name = 'order_requested.html'

    def get_context_data(self, *args, **kwargs):
        ctx = super(OrderRequested, self).get_context_data(*args, **kwargs)
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        return ctx


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class MyOrders(LoginRequiredMixin, ListView):
    template_name = 'my_orders.html'
    login_url = '/accounts/login/'
    paginate_by = 10

    def get_queryset(self):
        c = get_client_login(self.request)
        return Order.objects.filter(client=c).order_by('-date')

    def post(self, *args, **kwargs):
        return make_reorder(self.request)

    def get_context_data(self, *args, **kwargs):
        ctx = super(MyOrders, self).get_context_data(*args, **kwargs)
        c = get_client_login(self.request)
        ctx['category'] = Category.objects.all()
        ctx['client'] = c
        ctx['basket_quantity'] = ProductBasket.by_client(c).count()
        return ctx


@method_decorator(msgClientProfile, name='get')
@method_decorator(IsCycleOpen, name='dispatch')
class MyOrder(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = 'my_order.html'

    def get(self, *args, **kwargs):
        ctx = self.get_context_data()
        if ctx['order'].client == ctx['client']:
            return render(self.request,
                          self.template_name,
                          self.get_context_data())

        return redirect('my_orders')

    def post(self, *args, **kwargs):
        ctx = self.get_context_data()
        if ctx['order'].client == ctx['client']:
            return render(self.request,
                          self.template_name,
                          self.get_context_data())

        return redirect('my_orders')

    def get_context_data(self, *args, **kwargs):
        ctx = super(MyOrder, self).get_context_data(*args, **kwargs)
        o = Order.objects.get(pk=self.kwargs['pk'])
        ctx['category'] = Category.objects.all()
        ctx['client'] = get_client_login(self.request)
        ctx['basket_quantity'] = ProductBasket.by_client(ctx['client']).count()
        ctx['order'] = o
        ctx['products'] = ProductOrder.by_order(o)
        ctx['cycle'] = Cycle.get_cycle()
        return ctx


def make_reorder(request):
    client = get_client_login(request)
    ProductBasket.clean_basket(client)
    order = Order.objects.get(pk=int(request.POST['order']))
    products = ProductOrder.by_order(order)
    for p in products:
        if p.product.available:
            if p.quantity >= p.product.minimum_quantity:
                ProductBasket.add_product(client, p.product, p.quantity)
            else:
                ProductBasket.add_product(client, p.product, p.product.minimum_quantity)

    return redirect('client_basket')
