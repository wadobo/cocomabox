from django.contrib import admin
from order.models import Order, ProductOrder
from order.views import pdf_client_order_response

# Register your models here.


class ProductOrderInline(admin.TabularInline):
    model = ProductOrder

def pdf_order_action(modeladmin, request, queryset):
    return pdf_client_order_response(request, queryset)


pdf_order_action.short_description = "Generate pdf order"


def order_complete(modeladmin, request, queryset):
    queryset.update(state='2')


order_complete.short_description = "Order completed"


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['pk', 'client', 'date', 'total', 'state', 'pickup_day']
    list_filter = ['state', 'pickup_day']
    # search_fields = ['state', 'pickup_day']
    ordering = ['-date', '-pk']
    list_editable = ['state']
    actions = [order_complete, pdf_order_action]
    inlines = [ProductOrderInline, ]
