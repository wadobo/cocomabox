from django.conf.urls import url
from order.views import OrderRequested, MyOrders, MyOrder, make_reorder
from client.views import make_order

urlpatterns = [
    url(r'^pedido-realizado/$', OrderRequested.as_view(),
        name='order_requested'),
    url(r'^mis-pedidos/$', MyOrders.as_view(), name='my_orders'),
    url(r'^mis-pedidos/(?P<pk>\d+)$', MyOrder.as_view(), name='my_order'),
    url(r'^realizar-pedido/(\d+)$', make_order, name='make_order'),
    url(r'^reordenar-pedido/(\d+)$', make_reorder, name='make_reorder'),
]
