from django.db import models
from client.models import Client
from django.utils import timezone
from datetime import datetime
from client.models import ProductBasket
from product.models import Product, ProductDiscount
from utils.models import ProviderOrder, ProductProviderOrder, Cycle
from django.core.urlresolvers import reverse
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.core.mail import send_mail
from django.conf import settings
# Create your models here.


class Order(models.Model):
    STEP1 = '1'
    STEP2 = '2'

    STATES = (
        ('1', 'requested'),
        ('2', 'completed'),
        )

    client = models.ForeignKey(Client, related_name='order',
                               verbose_name='Cliente')
    date = models.DateTimeField(default=datetime.today(),
                                verbose_name='Fecha del pedido')
    total = models.FloatField(verbose_name='Total')
    total_discount = models.FloatField(verbose_name='Total con descuento', default=0.00)
    
    state = models.CharField(max_length=10,
                             choices=STATES, default=STEP1,
                             verbose_name='Estado')
    pickup_day = models.DateTimeField(auto_now=False,
                                      verbose_name='Día de recogida',
                                      default=None, null=True,
                                      blank=True)
    observations = models.TextField(blank=True, null=True,
                                    verbose_name='Observaciones')
    discount = models.ForeignKey(ProductDiscount, related_name='product_discount', null=True,
                                 verbose_name='Código de descuento', blank=True, default=None)

    def __str__(self):
        return '%s - %s %s' % (self.pk, self.date, self.client)

    class Meta:
        ordering = ["date"]

    def get_total(self):
        return round(self.total, 2)

    def get_products(self):
        return ProductOrder.objects.filter(order=self)

    def add_products(self):
        pb = ProductBasket.by_client(self.client)
        for p in pb:
            ProductOrder.objects.create(order=self,
                                        product=p.product,
                                        quantity=p.quantity)
            l_po = ProviderOrder.objects.filter(cycle=Cycle.get_cycle())
            po = l_po.filter(provider=p.product.provider).first()
            products_po = ProductProviderOrder.objects.all().filter(
                            providerOrder=po)
            product_exist = products_po.filter(product=p.product)
            if product_exist:
                new_product = product_exist.first()
                new_product.quantity += float(p.quantity)
                new_product.save()
            else:
                ProductProviderOrder.objects.create(providerOrder=po,
                                                    product=p.product,
                                                    quantity=p.quantity)
            po.set_total()

            po.save()
        ProductBasket.clean_basket(self.client)

    def get_absolute_url(self):
        view_name = 'my_order'
        return reverse(view_name, kwargs={"pk": self.pk})


class ProductOrder(models.Model):
    order = models.ForeignKey(Order, blank=True, verbose_name='Pedido')
    product = models.ForeignKey(Product, blank=True, verbose_name='Producto')
    quantity = models.FloatField(verbose_name='Cantidad')

    def by_order(order):
        return ProductOrder.objects.all().filter(order=order)

    def __str__(self):
        return "Unidad: %s" % (self.product.unit)


@receiver(post_save, sender=Order, dispatch_uid="email_order_complete")
def email_order_complete(sender, instance, **kwargs):
    if instance.state == '2':
        topic = 'Cocomabox Pedido ID: ' + str(instance.pk) + ' ' + \
                str(instance.date)
        message = 'Buenas ' + instance.client.first_name + ',\n\n'
        message += 'Su pedido con ID: ' + str(instance.pk) + \
            ' ha sido preparado.' + '\n'

        message += 'Le recordamos que puede recoger el pedido el día: ' +\
            instance.pickup_day.strftime("%d/%m/%y") + '\n\n'

        message += 'Le Adjuntamos el resumen de su pedido. \n\n'

        message += 'Atentamente, \n \tCocomá.'

        sender = str(instance.client.email)
        send_mail(topic, message, settings.DEFAULT_FROM_EMAIL, [sender])
